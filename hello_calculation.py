#!/usr/bin/env python
import sympy as sp
from Qs import Q, Qs, add

q1 = Q([1, 2, 3, 4])
t, x, y, z = sp.symbols("t x y z")
q2 = Q([t, x, y, z])

q1.print_state("q1")
q2.print_state("q2")
add(q1, q2).print_state("q1+q2")

q1q2q12 = Qs([q1, q2, add(q1, q2)])
q1q2q12.print_state("Quaternion state: q1, q2, q1+q2")
