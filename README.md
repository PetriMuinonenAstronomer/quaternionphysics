# Quaternion Physics

A place to discuss and create tools for using quaternions to replace tensors in all of physics.

Here is a simple calculation using the Qs.py library for the classes Q for
quaternions a division algebra and Qs for a quaternion series a semi-group
with inverses. The code was rewritten to reflect a more functional programming
style so that functions are on the same level as the classes Q and Qs instead
of being their methods. This is more consistent with how mathematicians view
functions. So for example, one would write code sin(q) in the functional style 
instead of q.sin() with an object oriented approach.
```
> hello_calculation.py
q1
(1, 2, 3, 4)

q2
(t, x, y, z)

q1+q2
(t + 1, x + 2, y + 3, z + 4)

Quaternion state: q1, q2, q1+q2
n=1: (1, 2, 3, 4)
n=2: (t, x, y, z)
n=3: (t + 1, x + 2, y + 3, z + 4)
ket: 3/1
```
## Branches of Physics

In classical Newtonian physics, time and space are as Newton described so many 
years ago, separate from each other. Einstein's work on special relativity and
Minkowski's insights led to the idea that time can rotate into space for different
inertial observers so long as the real-valued interval is the same. A new proposal
for how gravity works postulates that in tangent spaces of space-time such as
energy-momentum, the imaginary space-times-time value is something agreed upon by
two observers located at different places in a gravitational field. Quantum
mechanics is the study of the world where a space and its tangent space have to
fully embrace using Hilbert space of quaternion series states.
![](images/banner_w_labels.png)
