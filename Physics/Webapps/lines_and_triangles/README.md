# Webapp for Lines and Triangles

Here is the code for a webapp that uses the quaternion qs.py library. It is an 
exploration of a quaternion cross product.

[The code is live on the web!](https://lines-and-triangles.herokuapp.com/)

The webapp can also be run locally using the following command, assuming one
has run 'pip install -r requirements.txt' successfully.

```
> streamlit run webapp__lines_triangles_and_cross_products.py
...
```
In a few seconds, the app should appear in your default browser.

A cross product is about three points in 3D space. If they are in a straight line,
then then cross product is zero. The cross product reaches its maximal value
when one of the angles is 90 degrees.