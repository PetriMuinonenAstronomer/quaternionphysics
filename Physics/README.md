# Doing Physics with Quaternions

The maintainer of this site (Doug Sweetser) is also responsible for
[quaternions.com](http://quaternions.com). It is impractical to do all of
physics using quaternions. Instead, the focus will be on particular technical
subjects which people have made claims that quaternions are an unmixed evil or
complete dumpster fire.

According only to Mr. Sweetser, Mr. Sweetser is not a zealot on quaternions. Rather
he thinks he is an analytic puzzle solver. If something can be done with real
and/or complex numbers, then it can be done with quaternions because both of
those mathematical fields are contained in the division algebra of quaternions.
Quaternions are not a mathematical field because multiplication does not
commute. This translates to be able to solve problems like a rotating bicycle
wheel and problems involving angular momentum in general.

What about quantum mechanics? Quaternion series is a mathematical structure
being investigated to do the work. A quaternion series has a number of rows and
columns whose product is the dimension of the series. The series can be
infinite. A quaternion series is not a division algebra. One way to see this is
that two non-zero quaternion series can be orthagonal so their dot product is
zero. A quaternion series is a semi-group with inverses. There are ['2^n']
possible identities for a quaternion series of n dimensions.

Much work on the foundations of quantum mechanics uses [the Pauli matrices](https://en.wikipedia.org/wiki/Pauli_matrices). A
factor of _i_ times the three sigmas is isomorphic to the quaternions. The
Pauli matrices can then be used for a Lie algebra to do the work of the Lorentz
group. So while it may be possible in theory to use real-valued quaternions to
do all that is already done with the Lorentz group, the details matter.

## The Equivalence Relation Rule

Physics is a mature science. For this project to be valid, every equation
supported by experimental evidence must have an equivalence relation to an
expression written using quaternions. As will be seen time and time again, the
reason one looks for an equivalence relation instead of identical equations is that
quaternion expressions often have three more terms than appear using standard
tools. Those extra terms are as well-formed as what has been tested and
accepted in standard physics. Getting three new handles on how Nature works
could be a good thing.

## Why Bother?

If one uses the equivalence relation rule, it is a promise that that no
experimental test of standard physics can even in theory be different from its
quaternion counterpart. While consistency is a good thing, this level of
consistency makes the project moot since nothing new can be seen. Only if new
things can be seen would the project have a formal posibility of being of
interest.

It is the extra terms that appear next to standard physics expressions that
should excite the shit out of theoretical physicists (oops, need to curb my
enthusiasm). Most of the deep riddles that remain in physics have been around
since the 1920s. There are a few modern puzzlers involving the standard model
(early 1970s). 

1. Gravity
2. Fundamental forces and time reversal
3. Visualizations
4. Quantum mechanics

In time I will add content for all of these issues.
